package main

import (
	"fmt"
	"github.com/Shopify/sarama"
	"log"
	"strings"
	"time"
)

func main() {
	brokerList := []string{
		"localhost:9092",
	}

	log.Printf("Kafka brokers: %s", strings.Join(brokerList, ", "))
	log.Print("Initializing the Producer")

	producer, _ := createProducer(brokerList)

	// Just send some shit over
	message := &sarama.ProducerMessage{
		Topic: "testing-events",
		Value: sarama.StringEncoder("{\"meta\":{\"name\":\"PersonAddressChanged\",\"timestamp\":\"2019-07-05T13:25:57.101Z\",\"correlation_id\":\"123456789\",\"distribution_key\":\"abc\"},\"messageValue\":\"Works\"},\"messageKey\":\"12345\"}"),
	}

	var err interface{}

	_, _, err = producer.SendMessage(message)

	if err != nil {
		fmt.Println(err)
	}

}

func createProducer(brokers []string) (sarama.SyncProducer, error) {
	// For the access log, we are looking for AP semantics, with high throughput.
	// By creating batches of compressed messages, we reduce network I/O at a cost of more latency.
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll         // Wait for all replicas
	config.Producer.Compression = sarama.CompressionSnappy   // Compress messages
	config.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms
	config.Producer.Return.Successes = true

	producer, err := sarama.NewSyncProducer(brokers, config)
	if err != nil {
		log.Fatalln("Failed to start Sarama producer:", err)
	}

	return producer, err
}

func ProduceMessage() {

}

//func (s *Server) Close() error {
//	if err := s.DataCollector.Close(); err != nil {
//		log.Println("Failed to shut down data collector cleanly", err)
//	}
//
//	if err := s.AccessLogProducer.Close(); err != nil {
//		log.Println("Failed to shut down access log producer cleanly", err)
//	}
//
//	return nil
//}
//
//func (s *Server) collectQueryStringData() http.Handler {
//	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		if r.URL.Path != "/" {
//			http.NotFound(w, r)
//			return
//		}
//
//		// We are not setting a message key, which means that all messages will
//		// be distributed randomly over the different partitions.
//		partition, offset, err := s.DataCollector.SendMessage(&sarama.ProducerMessage{
//			Topic: "important",
//			Value: sarama.StringEncoder(r.URL.RawQuery),
//		})
//
//		if err != nil {
//			w.WriteHeader(http.StatusInternalServerError)
//			fmt.Fprintf(w, "Failed to store your data:, %s", err)
//		} else {
//			// The tuple (topic, partition, offset) can be used as a unique identifier
//			// for a message in a Kafka cluster.
//			fmt.Fprintf(w, "Your data is stored with unique identifier important/%d/%d", partition, offset)
//		}
//	})
//}

//func newDataCollector(brokerList []string) sarama.SyncProducer {
//
//	// For the data collector, we are looking for strong consistency semantics.
//	// Because we don't change the flush settings, sarama will try to produce messages
//	// as fast as possible to keep latency low.
//	config := sarama.NewConfig()
//	config.Producer.RequiredAcks = sarama.WaitForAll // Wait for all in-sync replicas to ack the message
//	config.Producer.Retry.Max = 10                   // Retry up to 10 times to produce the message
//	config.Producer.Return.Successes = true
//	tlsConfig := createTlsConfiguration()
//	if tlsConfig != nil {
//		config.Net.TLS.Config = tlsConfig
//		config.Net.TLS.Enable = true
//	}
//
//	// On the broker side, you may want to change the following settings to get
//	// stronger consistency guarantees:
//	// - For your broker, set `unclean.leader.election.enable` to false
//	// - For the topic, you could increase `min.insync.replicas`.
//
//	producer, err := sarama.NewSyncProducer(brokerList, config)
//	if err != nil {
//		log.Fatalln("Failed to start Sarama producer:", err)
//	}
//
//	return producer
//}
//
//func newAccessLogProducer(brokerList []string) sarama.AsyncProducer {
//
//	// For the access log, we are looking for AP semantics, with high throughput.
//	// By creating batches of compressed messages, we reduce network I/O at a cost of more latency.
//	config := sarama.NewConfig()
//	tlsConfig := createTlsConfiguration()
//	if tlsConfig != nil {
//		config.Net.TLS.Enable = true
//		config.Net.TLS.Config = tlsConfig
//	}
//	config.Producer.RequiredAcks = sarama.WaitForLocal       // Only wait for the leader to ack
//	config.Producer.Compression = sarama.CompressionSnappy   // Compress messages
//	config.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms
//
//	producer, err := sarama.NewAsyncProducer(brokerList, config)
//	if err != nil {
//		log.Fatalln("Failed to start Sarama producer:", err)
//	}
//
//	// We will just log to STDOUT if we're not able to produce messages.
//	// Note: messages will only be returned here after all retry attempts are exhausted.
//	go func() {
//		for err := range producer.Errors() {
//			log.Println("Failed to write access log entry:", err)
//		}
//	}()
//
//	return producer
//}
